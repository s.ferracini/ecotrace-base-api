"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddNameToUsersSchema extends Schema {
  up() {
    this.table("users", table => {
      table
        .string("name")
        .notNullable()
        .after("id")
        .defaultTo(" ");
    });
  }

  down() {}
}

module.exports = AddNameToUsersSchema;
