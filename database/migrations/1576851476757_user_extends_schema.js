"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UserExtendsSchema extends Schema {
  up() {
    this.create("user_extends", table => {
      table.bigIncrements().unsigned();

      table
        .integer("user_id")
        .unsigned()
        .index();
      table.string("cpf").unique();
      table.text("picture");
      table.string("firebase_id");
      table.string("google_id");
      table.string("facebook_id");
      table.timestamps();

      table
        .foreign("user_id")
        .references("id")
        .on("users")
        .onDelete("cascade");
    });
  }

  down() {
    this.drop("user_extends");
  }
}

module.exports = UserExtendsSchema;
