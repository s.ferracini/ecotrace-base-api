# Iniciando com AdonisJS

O AdonisJS é um framework para NodeJS que utiliza o padrão MVC como arquitetura.

Para criar um projeto, primeiro baixe o CLI (Command Line Interface) do Adonis.

```bash
npm i -g @adonisjs/cli
```

Em uma pasta de sua escolha, execute o comando a seguir para criar a pasta de seu projeto.

```js
adonis new nome-projeto-api --api-only
```

O '--api-only' no fim significa que queremos apenas a parte de API do Adonis.

Abra a pasta do projeto e execute o comando a seguir:

```bash
adonis serve --dev
```

Esse comando basicamente iniciará nosso servidor de desenvolvimento que agora pode ser acessado no endereço http://localhost:3333.

Um ponto extremamente importante é que toda camada de banco de dados é abstraída pelo Lucid ORM, ou seja, utilizaremos métodos disponíveis nos models para realizar as queries.

Grande parte das aplicações que desenvolveremos utilizarão as bibliotecas a seguir, as instale com os comandos a seguir:

```bash
npm install pg
npm i adonis-acl --save
adonis install adonis-bumblebee
adonis install adonis-lucid-filter
```

Siga até o passo de número 5 no primeiro comando, em https://github.com/enniel/adonis-acl para registrar os providers

Siga o passo `Usage` em https://github.com/lookinlab/adonis-lucid-filter

Após a instalação e configuração, crie um arquivo na raiz de seu projeto chamado `.env` afim de centralizar os dados de acesso ao banco de dados. (Casi esteja criado, não precisa)

Copiei e cole o código a seguir no arquivo `.env`

```bash
DB_CONNECTION=pg
DB_HOST=localhost
DB_PORT=5432
DB_USER=postgres
DB_PASSWORD=
DB_DATABASE=portal
```

OBS: Verifique o ip e porta publicada em sua máquina pelo docker, o nome, o usuário e a senha do banco de dados previamente instalado e configurado

Após configurados as bibliotecas rode o comando a seguir para criar as migrations:

```bash
adonis acl:setup
```

Você obterá algo como

```bash
database/migrations/1576784519439_create_permissions_table.js
database/migrations/1576784519443_create_roles_table.js
database/migrations/1576784519444_create_permission_role_table.js
database/migrations/1576784519445_create_permission_user_table.js
database/migrations/1576784519445_create_role_user_table.js
```

Altere o código da migration user para:

```js
"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UserSchema extends Schema {
  up() {
    this.create("users", table => {
      table.increments();
      table
        .string("username", 254)
        .notNullable()
        .unique();
      table
        .string("email", 254)
        .notNullable()
        .unique();
      table
        .string("phone", 255)
        .notNullable()
        .unique();
      table.string("password", 60).notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("users");
  }
}

module.exports = UserSchema;
```

Execute o comando a seguir para adicionar o campo nome na tabela de usuários, selecionando a opção `select table`:

```bash
adonis make:migration add_name_to_users
```

Copie o código a seguir para o arquivo criado

```js
"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddNameToUsersSchema extends Schema {
  up() {
    this.table("users", table => {
      table
        .string("name")
        .notNullable()
        .after("id")
        .defaultTo(" ");
    });
  }

  down() {}
}

module.exports = AddNameToUsersSchema;
```

Execute o comando a seguir para criar a tabela de informações do usuário, selecionando a opção `create table`:

```bash
adonis  make:migration user_extends
```

Copie o código a seguir para o arquivo

```js
"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UserExtendsSchema extends Schema {
  up() {
    this.create("user_extends", table => {
      table.bigIncrements().unsigned();

      table
        .integer("user_id")
        .unsigned()
        .index();
      table.string("cpf").unique();
      table.text("picture");
      table.string("firebase_id");
      table.string("google_id");
      table.string("facebook_id");
      table.timestamps();

      table
        .foreign("user_id")
        .references("id")
        .on("users")
        .onDelete("cascade");
    });
  }

  down() {
    this.drop("user_extends");
  }
}

module.exports = UserExtendsSchema;
```

Foram criadas todas as migrations, execute o comando a seguir para criar as tabelas:

```bash
adonis migration:run
```

Crie o model UserExtends executando o comando

```bash
adonis make:model UserExtend
```

Abra o arquivo e cole o seguinte código

```js
"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class UserExtend extends Model {
  static fillable() {
    return ["user_id", "cpf", "firebase_id", "google_id", "facebook_id"];
  }

  user() {
    return this.belongsTo("App/Models/User").fetch();
  }
}

module.exports = UserExtend;
```

Rode o comando a seguir para criar o filtro do usuário

```bash
adonis make:modelFilter User
```

Altere o conteúdo do arquivo para:

```js
"use strict";

const ModelFilter = use("ModelFilter");

class UserFilter extends ModelFilter {
  id(id) {
    return this.where("id", +id);
  }

  name(name) {
    return this.whereRaw(
      "LOWER(name) like LOWER(?)",
      "%" + name.trim().toLowerCase() + "%"
    );
  }

  email(email) {
    return this.whereRaw(
      "LOWER(email) like LOWER(?)",
      "%" + email.trim().toLowerCase() + "%"
    );
  }

  documentNumber(documentNumber) {
    return this.whereHas("userExted", builder => {
      builder.where("cpf", documentNumber);
    });
  }
}

module.exports = UserFilter;
```

Rode `adonis make:seed PermissionSeeder` e substitua o conteúdo do arquivo criado por o código a seguir:

```js
"use strict";

const Role = use("Adonis/Acl/Role");
const Permission = use("Adonis/Acl/Permission");

/*
|--------------------------------------------------------------------------
| PermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class PermissionSeeder {
  async createRole(name) {
    let role = await Role.query()
      .where("slug", "=", name)
      .first();
    if (!role) {
      role = new Role();
      role.name = name;
      role.slug = name;
      role.description = name;
      await role.save();
    }

    return role;
  }

  async createPermission(name) {
    let permission = await Permission.query()
      .where("slug", "=", name)
      .first();
    if (!permission) {
      permission = new Permission();
      permission.slug = name;
      permission.name = name;
      permission.description = name;
      await permission.save();
    }

    return permission;
  }

  async generatePermissions(name) {
    let permissions = {};
    let pType = ["list", "create", "edit", "delete"];
    for (let i = 0; i < 4; i++) {
      permissions[pType[i]] = await this.createPermission(
        pType[i] + "_" + name
      );
    }

    return permissions;
  }

  async addPermissions(types, role, permissions) {
    if (!types) {
      types = ["list", "create", "edit", "delete"];
    }

    for (let i = 0; i < types.length; i++) {
      await role.permissions().attach([permissions[types[i]].id]);
    }
  }

  async run() {
    //roles
    const adminRole = await this.createRole("admin");
    const userRole = await this.createRole("user");
    const industry = await this.createRole("industry");

    //permissions
    const users = await this.generatePermissions("user");
    const aux = await this.generatePermissions("aux");

    //add permissions

    //admin
    await this.addPermissions(null, adminRole, users);
    await this.addPermissions(null, adminRole, aux);
  }
}

module.exports = PermissionSeeder;
```

Rode `adonis make:seed DefaultUser` e substitua o conteúdo do arquivo criado por o código a seguir:

```js
"use strict";

const User = use("App/Models/User");
const UserExtend = use("App/Models/UserExtend");
const Role = use("Adonis/Acl/Role");
const Permission = use("Adonis/Acl/Permission");

/*
|--------------------------------------------------------------------------
| DefaultUserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

class DefaultUserSeeder {
  async createDefaultUser() {
    let defaultUser = await User.query()
      .where("email", "=", "ti@ecotrace.info")
      .first();
    const roleAdmin = await Role.query()
      .where("slug", "=", "admin")
      .first();
    const roleUser = await Role.query()
      .where("slug", "=", "user")
      .first();

    if (!defaultUser) {
      defaultUser = new User();
      defaultUser.name = "default";
      defaultUser.phone = "99999999999";
      defaultUser.email = "ti@ecotrace.info";
      defaultUser.password = "a1s2d3f4";
      defaultUser.username = "ti@ecotrace.info";

      await defaultUser.save();
      await defaultUser.roles().attach([roleAdmin.id, roleUser.id]);

      let userCpf = new UserExtend();
      userCpf.cpf = "31197359060";
      userCpf.user_id = defaultUser.id;

      await userCpf.save();
    }

    return defaultUser;
  }

  async run() {
    await this.createDefaultUser();
  }
}

module.exports = DefaultUserSeeder;
```

Rode as seeds com:

```bash
 adonis seed --files "PermissionSeeder.js"
 adonis seed --files "DefaultUserSeeder.js"
```

Crie o formatador de data no caminho `/src/app/Helpers/index.js` e cole o conteúdo a seguir:

```js
"use strict";

const moment = require("moment");

const format_date = (date, format) => {
  format = format ? format : "YYYY-MM-DD HH:mm:ss";
  if (moment(date).isValid()) {
    return moment(date).format(format);
  }
  return "";
};

module.exports = {
  format_date
};
```

Agora, criaremos o controller de autenticação, execute o comando a seguir para a criação do controller

```bash
adonis make:controller Auth --type http
```

Foi criado em `app/Controllers/Http` o arquivo `AuthController.js` que será utilizado para login.

Altere o arquivo para:

```js
"use strict";

const User = use("App/Models/User");
const UserExted = use("App/Models/UserExtend");
const Database = use("Database");
const Role = use("Role");

class AuthController {
  async register({ request, transform }) {
    const { authToken } = request.all();

    let usetExtedData = request.only(UserExted.fillable());

    const trx = await Database.beginTransaction();

    try {
      if (authToken) {
        const userToken = await FirebaseAdmin.admin
          .auth()
          .verifyIdToken(authToken);

        if (userToken) {
          usetExtedData["firebase_id"] = userToken.user_id;
          usetExtedData["picture"] = userToken.picture;
        }
      }

      const user = await User.create(request.only(User.fillable()));
      const role = await Role.query()
        .where("slug", "=", "user")
        .firstOrFail();

      await user.roles().attach([role.id]);
      usetExtedData["user_id"] = user.id;
      await UserExted.create(usetExtedData);

      await trx.commit();
      try {
        await new SendGridMail()
          .to(user.email)
          .subject("Bem-vindo a ecotrace!")
          .view("emails.welcome", {
            user: user,
            portalLink: "https://web.ecotrace.solutions"
          })
          .sendEmail();
      } catch (error) {}

      return transform.item(user, "UserTransformer");
    } catch (error) {
      await trx.rollback();
      throw error;
    }
  }

  async login({ auth, request, response }) {
    const { profile, email, password, authToken, force } = request.all();

    let userToken = null;
    let user = null;

    let refreshToken = request.header("Authorization");
    if (refreshToken) {
      refreshToken = refreshToken.replace("Bearer ", "");
      userToken = await auth.generateForRefreshToken(refreshToken, true);
    } else {
      if (!userToken && email && password) {
        userToken = auth.withRefreshToken().attempt(email, password);
      }
    }

    if (!userToken) {
      return response
        .status(401)
        .json({ message: "Usuário ou senha inválido" });
    }

    if (profile) {
      if (!user) {
        user = await User.query()
          .where("email", email)
          .first();
        if (!(await user.is(profile))) {
          return response
            .status(401)
            .json({ message: "Usuário ou senha inválido, sem acesso." });
        }
      }
    }

    return userToken;
  }

  async refresh({ auth, request }) {
    let refreshToken = request.header("Authorization");
    if (refreshToken) {
      refreshToken = refreshToken.replace("Bearer ", "");
    }
    return await auth.generateForRefreshToken(refreshToken, true);
  }

  async user({ auth, params, transform }) {
    return transform.item(auth.user, "UserTransformer");
  }

  async logout({ auth, response }) {
    try {
      return await auth.logout();
    } catch (error) {}
    response.send({});
  }

  async changePassword({ auth, request }) {}
}

module.exports = AuthController;
```

Com o controller de autenticação criado, temos que criar as rotas, para isso no arquivo `start/routes.js` altere o conteúdo para:

```js
"use strict";

/** .type {typeof import('.adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.group(() => {
  // Route.post('register', 'AuthController.register').validator('StoreUser');
  Route.post("login", "AuthController.login");
  // Route.get('refresh', 'AuthController.refresh');
  Route.post("logout", "AuthController.logout");
  // Route.get('user', 'AuthController.user');
}).prefix("api/v1/auth");
```

Antes de testarmos, caso haja o arquivo `/config/shield.js`, altere o final do arquivo para:

```js
  csrf: {
    enable: false,
    methods: ["POST", "PUT", "DELETE"],
    filterUris: [],
    cookieOptions: {
      httpOnly: false,
      sameSite: true,
      path: "/",
      maxAge: 7200
    }
  }
```

Altere também o `authenticator` no arquivo `/config/auth.js` para `'jwt'`
