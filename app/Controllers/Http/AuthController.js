"use strict";

const User = use("App/Models/User");
// const UserExted = use("App/Models/UserExtend");
// const FirebaseAdmin = use('FirebaseAdmin')
const Database = use("Database");
const Role = use("Role");
// const SendGridMail = use('App/Services/SendGridMail');

class AuthController {
  // async register({ request, transform }) {
  //   const { authToken } = request.all();

  //   let usetExtedData = request.only(UserExted.fillable());

  //   const trx = await Database.beginTransaction();

  //   try {
  //     if (authToken) {
  //       const userToken = await FirebaseAdmin.admin
  //         .auth()
  //         .verifyIdToken(authToken);

  //       if (userToken) {
  //         usetExtedData["firebase_id"] = userToken.user_id;
  //         usetExtedData["picture"] = userToken.picture;
  //       }
  //     }

  //     const user = await User.create(request.only(User.fillable()));
  //     const role = await Role.query()
  //       .where("slug", "=", "user")
  //       .firstOrFail();

  //     await user.roles().attach([role.id]);
  //     usetExtedData["user_id"] = user.id;
  //     await UserExted.create(usetExtedData);

  //     await trx.commit();
  //     try {
  //       await new SendGridMail()
  //         .to(user.email)
  //         .subject("Bem-vindo a ecotrace!")
  //         .view("emails.welcome", {
  //           user: user,
  //           portalLink: "https://web.ecotrace.solutions"
  //         })
  //         .sendEmail();
  //     } catch (error) {}

  //     return transform.item(user, "UserTransformer");
  //   } catch (error) {
  //     await trx.rollback();
  //     throw error;
  //   }
  // }

  async login({ auth, request, response }) {
    const { profile, email, password, authToken, force } = request.all();

    let userToken = null;
    let user = null;

    //login com refresh token
    let refreshToken = request.header("Authorization");
    if (refreshToken) {
      refreshToken = refreshToken.replace("Bearer ", "");
      userToken = await auth.generateForRefreshToken(refreshToken, true);
    } else {
      //login usuário e senha
      if (!userToken && email && password) {
        userToken = auth.withRefreshToken().attempt(email, password);
      }
    }

    if (!userToken) {
      return response
        .status(401)
        .json({ message: "Usuário ou senha inválido" });
    }

    if (profile) {
      if (!user) {
        user = await User.query()
          .where("email", email)
          .first();
        if (!(await user.is(profile))) {
          return response
            .status(401)
            .json({ message: "Usuário ou senha inválido, sem acesso." });
        }
      }
    }

    return userToken;
  }

  async refresh({ auth, request }) {
    let refreshToken = request.header("Authorization");
    if (refreshToken) {
      refreshToken = refreshToken.replace("Bearer ", "");
    }
    return await auth.generateForRefreshToken(refreshToken, true);
  }

  async user({ auth, params, transform }) {
    return transform.item(auth.user, "UserTransformer");
  }

  async logout({ auth, response }) {
    try {
      return await auth.logout();
    } catch (error) {}
    response.send({});
  }

  async changePassword({ auth, request }) {}
}

module.exports = AuthController;
