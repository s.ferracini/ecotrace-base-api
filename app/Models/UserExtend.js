"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class UserExtend extends Model {
  static fillable() {
    return ["user_id", "cpf", "firebase_id", "google_id", "facebook_id"];
  }

  user() {
    return this.belongsTo("App/Models/User").fetch();
  }
}

module.exports = UserExtend;
