"use strict";

const moment = require("moment");

const format_date = (date, format) => {
  format = format ? format : "YYYY-MM-DD HH:mm:ss";
  if (moment(date).isValid()) {
    return moment(date).format(format);
  }
  return "";
};

module.exports = {
  format_date
};
